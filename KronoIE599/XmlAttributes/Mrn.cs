﻿using System;

namespace KronoIE599.XmlAttributes
{
    public class Mrn
    {
        public string Value;
        public bool IsCorrect;

        public Mrn()
        {
        }

        public Mrn(string value, bool isCorrect)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            IsCorrect = isCorrect;
        }
    }
}