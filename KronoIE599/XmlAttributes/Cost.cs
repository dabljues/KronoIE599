﻿namespace KronoIE599.XmlAttributes
{
    public class Cost
    {
        public enum Currency
        {
            Eur,
            Pl
        }

        public double Value;
        public bool IsCorrect;
        public Currency CostCurrency;

        public Cost()
        {
        }

        public Cost(double value, bool isCorrect, Currency costCurrency)
        {
            Value = value;
            IsCorrect = isCorrect;
            CostCurrency = costCurrency;
        }
    }
}