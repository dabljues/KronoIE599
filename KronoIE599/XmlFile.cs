﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using KronoIE599.XmlAttributes;

namespace KronoIE599
{
    public class XmlFile
    {
        // Obiekt obsługujący plik XML
        private readonly XDocument _doc;

        // Unikalne ID nadawane każdemu plikowi (nie ma to nic wspólnego z plikami XML)
        public string UniqueId;

        // Atrybuty XML
        public string FileName;

        public List<string> InvoiceNumbers;
        public Mrn Mrn;
        public Cost Cost;
        public string StringCurrency;

        public XmlFile(string uniqueId)
        {
            UniqueId = uniqueId;
            SetUpAttributes();
            InvoiceNumbers = new List<string>();
        }

        public XmlFile(string path, string uniqueId)
        {
            // Otworzenie pliku XML
            _doc = XDocument.Load(path);
            // Wyłuskanie nazwy pliku z pełnej ścieżki do pliku
            FileName = path.Split('\\').Last();
            UniqueId = uniqueId;
            SetUpAttributes();
            InvoiceNumbers = new List<string>();
        }

        /// <summary>
        ///     Funkcja inicjalizuje atrybuty klasy (gdyż są to osobne klasy, nie typy proste)
        /// </summary>
        private void SetUpAttributes()
        {
            Mrn = new Mrn();
            Cost = new Cost();
        }

        /// <summary>
        ///     Funkcja zwraca tag XML, czyli <costam></costam>, w którym będziemy czytać atrybuty
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public XElement GetTag(string tag)
        {
            return _doc.Descendants().Select(x => x).FirstOrDefault(y => y.Name.LocalName == tag);
        }

        /// <summary>
        ///     Funkcja zwraca iterabla zawierającego numery faktur z pliku
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public IEnumerable<XElement> GetInvoices(string tag)
        {
            return _doc.Descendants().Select(x => x)
                .Where(y => y.Name.LocalName == tag &&
                            (y.Attribute("Kod")?.Value == "N380" || y.Attribute("Kod")?.Value == "N325"));
        }

        /// <summary>
        ///     Funkcja czyta plik XML wpisując do atrybutów klasy przeczytane dane
        /// </summary>
        /// <returns></returns>
        public bool Read599File()
        {
            var ret = true;
            var tag = GetTag("PotwierdzenieWywozu");
            Mrn.Value = tag.Attribute("MRN")?.Value;
            tag = GetTag("Transakcja");
            var value = tag.Attribute("Wartosc")?.Value;
            if (value != null)
            {
                try
                {
                    Cost.Value = double.Parse(value);
                }
                catch (FormatException)
                {
                    MessageBox.Show(
                        $@"Nazwa pliku: {FileName}{Environment.NewLine}Koszt: {value}{Environment.NewLine}");
                    throw;
                }
            }
            else
            {
                Cost.Value = 0.0;
                ret = false;
            }
            var currency = tag.Attribute("Waluta")?.Value;
            Cost.CostCurrency = currency == null ? Cost.Currency.Pl : Cost.Currency.Eur;
            StringCurrency = currency ?? "PLN";
            var invoices = GetInvoices("DokumentWymagany");
            var uniqueInvoices = new HashSet<string>();
            foreach (var invoice in invoices)
            {
                var number = invoice.Attribute("Nr")?.Value;
                if (uniqueInvoices.Contains(number))
                    continue;
                uniqueInvoices.Add(number);
                InvoiceNumbers.Add(number);
            }
            return ret;
        }
    }
}