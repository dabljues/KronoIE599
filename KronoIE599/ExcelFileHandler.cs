﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Spreadsheet;
using KronoIE599.XmlAttributes;
using SpreadsheetLight;
using Color = System.Drawing.Color;

namespace KronoIE599
{
    public class ExcelFileHandler
    {
        // Nazwa szablonu, z którego będziemy korzystać do wygenerowania Excela
        private string TemplateName = string.Empty;

        // Nazwa pliku, który zostanie wygenerowany
        private string WrittenFilePath = string.Empty;

        // Obiekt umożliwiający manipulację plikiem Excela
        private SLDocument _sl;

        private SLStyle _wrongCostStyle;

        private void GetTemplateNameFromDirectory()
        {
            var dir = "od ksiegowej";
            var path = Path.GetFullPath(dir);
            try
            {
                TemplateName = Path.GetFullPath(Directory.GetFiles(dir).First());
                WrittenFilePath = Path.GetFileNameWithoutExtension(TemplateName);
            }
            catch (Exception)
            {
                MessageBox.Show($@"W folderze {dir} nie ma żadnych plików!");
            }
        }

        /// <summary>
        ///     Funkcja ustawia styl używany do oznaczania komórek, gdzie wystąpiły błędy
        /// </summary>
        private void SetUpWrongCellStyle()
        {
            _wrongCostStyle = _sl.CreateStyle();
//            _wrongCostStyle.SetFontColor(Color.Red);
            _wrongCostStyle.Fill.SetPattern(PatternValues.Solid, Color.IndianRed, Color.DarkSalmon);
        }

        /// <summary>
        ///     Funkcja zwraca nazwę pliku wyjściowego (bez rozszerzenia)
        /// </summary>
        /// <returns></returns>
        public string GetFileName()
        {
            return WrittenFilePath;
        }

        /// <summary>
        ///     Otworzenie szablonu do edycji (będzie on nietknięty -> Nie Save, a Save As)
        /// </summary>
        /// <returns></returns>
        public bool OpenTemplate()
        {
            try
            {
                _sl = new SLDocument(TemplateName);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show(string.IsNullOrEmpty(TemplateName)
                    ? $@"Nie znaleziono pliku {TemplateName}!"
                    : $@"Przed generacją zamknij plik {TemplateName}!");

                return false;
            }
        }

        /// <summary>
        ///     Funkcja zapisuje wygenerowany plik Excela
        /// </summary>
        /// <returns></returns>
        public bool SaveExcelFile()
        {
            var excelName = $"{WrittenFilePath}.xlsx";
            try
            {
                _sl.SaveAs(Path.GetFullPath(excelName));
            }
            catch (IOException)
            {
                MessageBox.Show($@"Przed generacją pliku Excela zamnkij plik {Path.GetFullPath(excelName)}!");
                return false;
            }
            return OpenTemplate();
        }

        /// <summary>
        ///     Funkcja wypełnia skoroszyt danymi (w pamięci)
        /// </summary>
        /// <param name="xmlPath"></param>
        public bool WriteExcelFile(string xmlPath)
        {
            GetTemplateNameFromDirectory();
            if (!OpenTemplate()) return false;
            SetUpWrongCellStyle();
            var files599 = Directory.GetFiles(xmlPath);
            var uniqueId = 0;
            const int rowIndex = 5;
            const int mrnColIndex = 8;
            var maxRowIndex = _sl.GetWorksheetStatistics().EndRowIndex;
            foreach (var file in files599)
            {
                var first = true;
                var firstIndex = -1;
                var xmlFile = new XmlFile(file, uniqueId.ToString());
                xmlFile.Read599File();
                var currencyColumnIndex = xmlFile.Cost.CostCurrency == Cost.Currency.Eur ? 6 : 5;
                xmlFile.InvoiceNumbers.Sort();
                var overallCost = xmlFile.Cost.Value;
                var indexes = new List<int>();
                foreach (var invoiceNumber in xmlFile.InvoiceNumbers)
                    for (var i = rowIndex; i < maxRowIndex; i++)
                    {
                        if (_sl.GetCellValueAsString(i, 2) != invoiceNumber) continue;
                        if (first)
                        {
                            firstIndex = i;
                            first = false;
                        }
                        _sl.SetCellValue(i, mrnColIndex, xmlFile.Mrn.Value);
                        var costFromExcel = double.Parse(_sl.GetCellValueAsString(i, currencyColumnIndex));
                        overallCost -= costFromExcel;
                        indexes.Add(i);
                        break;
                    }
                if (indexes.Count == 0) continue;
                var changeColor = false;
                if (Math.Abs(overallCost) <= 0.01 && !first)
                    overallCost = 0.00;
                else
                    changeColor = true;
                _sl.SetCellValue(firstIndex, mrnColIndex + 1, overallCost);
                var currencyFromXml = xmlFile.StringCurrency;
                var currencyFromExcel = _sl.GetCellValueAsString(firstIndex, 7);
                if (currencyFromXml != currencyFromExcel)
                {
                    _sl.SetCellValue(firstIndex, mrnColIndex + 2,
                        $"XML - {currencyFromXml}, Excel - {currencyFromExcel}");
                    _sl.SetCellStyle(firstIndex, mrnColIndex + 2, _wrongCostStyle);
                }
                if (changeColor) _sl.SetCellStyle(firstIndex, mrnColIndex + 1, _wrongCostStyle);
                uniqueId++;
            }
            return true;
        }
    }
}