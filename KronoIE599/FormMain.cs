﻿using System;
using System.IO;
using System.Windows.Forms;

namespace KronoIE599
{
    public partial class FormMain : Form
    {
        // Nazwa pliku konfiguracyjnego (ostatnia wybrana ścieżka)
        private const string ConfigFile = "konfiguracja.txt";

        // Ścieżka do folderu z plikami XML
        private string _xmlPath;

        // Obiekt do obłsugi pliku konfiguracyjnego
        private readonly ConfigReader _configReader;

        // Obiekt do obsługiwania pliku Excela
        private ExcelFileHandler _excelFileHandler;

        public FormMain()
        {
            InitializeComponent();
            buttonGenerate.Enabled = false;
            _configReader = new ConfigReader();
        }

        private void buttonSelectXMLPath_Click(object sender, EventArgs e)
        {
            var selectedPath = _configReader.GetXmlPath(ConfigFile);
            if (selectedPath != string.Empty && Directory.Exists(selectedPath))
            {
                File.WriteAllText(Path.GetFullPath(ConfigFile), selectedPath);
                buttonGenerate.Enabled = true;
            }
            _xmlPath = selectedPath;
            textBoxXMLPath.Text = _xmlPath;
        }

        private void EnableGUI(bool success)
        {
            buttonGenerate.Enabled = true;
            var value = success ? 100 : 0;
            progressBarGenerate.Value = value;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            _excelFileHandler = new ExcelFileHandler();
            buttonGenerate.Enabled = false;
            progressBarGenerate.Value = 0;
            if (!_excelFileHandler.WriteExcelFile(_xmlPath))
            {
                EnableGUI(false);
                return;
            }
            if (!_excelFileHandler.SaveExcelFile())
            {
                EnableGUI(false);
                return;
            }
            buttonGenerate.Enabled = true;
            progressBarGenerate.Value = 100;
        }
    }
}