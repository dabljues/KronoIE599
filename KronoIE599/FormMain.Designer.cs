﻿namespace KronoIE599
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxXMLPath = new System.Windows.Forms.TextBox();
            this.buttonSelectXMLPath = new System.Windows.Forms.Button();
            this.labelSelectXMLPath = new System.Windows.Forms.Label();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.progressBarGenerate = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // textBoxXMLPath
            // 
            this.textBoxXMLPath.Location = new System.Drawing.Point(210, 84);
            this.textBoxXMLPath.Name = "textBoxXMLPath";
            this.textBoxXMLPath.Size = new System.Drawing.Size(275, 20);
            this.textBoxXMLPath.TabIndex = 0;
            // 
            // buttonSelectXMLPath
            // 
            this.buttonSelectXMLPath.Location = new System.Drawing.Point(500, 82);
            this.buttonSelectXMLPath.Name = "buttonSelectXMLPath";
            this.buttonSelectXMLPath.Size = new System.Drawing.Size(26, 23);
            this.buttonSelectXMLPath.TabIndex = 1;
            this.buttonSelectXMLPath.Text = "...";
            this.buttonSelectXMLPath.UseVisualStyleBackColor = true;
            this.buttonSelectXMLPath.Click += new System.EventHandler(this.buttonSelectXMLPath_Click);
            // 
            // labelSelectXMLPath
            // 
            this.labelSelectXMLPath.AutoSize = true;
            this.labelSelectXMLPath.Location = new System.Drawing.Point(59, 87);
            this.labelSelectXMLPath.Name = "labelSelectXMLPath";
            this.labelSelectXMLPath.Size = new System.Drawing.Size(145, 13);
            this.labelSelectXMLPath.TabIndex = 2;
            this.labelSelectXMLPath.Text = "Wybierz folder z plikami XML:";
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(284, 135);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(96, 23);
            this.buttonGenerate.TabIndex = 3;
            this.buttonGenerate.Text = "Generuj";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // progressBarGenerate
            // 
            this.progressBarGenerate.Location = new System.Drawing.Point(210, 186);
            this.progressBarGenerate.Name = "progressBarGenerate";
            this.progressBarGenerate.Size = new System.Drawing.Size(275, 47);
            this.progressBarGenerate.TabIndex = 4;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 528);
            this.Controls.Add(this.progressBarGenerate);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.labelSelectXMLPath);
            this.Controls.Add(this.buttonSelectXMLPath);
            this.Controls.Add(this.textBoxXMLPath);
            this.Name = "FormMain";
            this.Text = "IE599";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxXMLPath;
        private System.Windows.Forms.Button buttonSelectXMLPath;
        private System.Windows.Forms.Label labelSelectXMLPath;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.ProgressBar progressBarGenerate;
    }
}

